package com.dohavatiuk.nenagera.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.dohavatiuk.nenagera.entity.OperationType;

@Repository
public interface OperationTypeRepository extends JpaRepository<OperationType, Long>{
	
}