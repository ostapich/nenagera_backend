package com.dohavatiuk.nenagera.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dohavatiuk.nenagera.entity.Pocket;

@Repository
public interface PocketRepository extends JpaRepository<Pocket, Long>{

}
