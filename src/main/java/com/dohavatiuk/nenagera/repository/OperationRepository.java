package com.dohavatiuk.nenagera.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dohavatiuk.nenagera.entity.Operation;

@Repository
public interface OperationRepository extends JpaRepository<Operation, Long> {

}
