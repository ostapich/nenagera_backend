package com.dohavatiuk.nenagera.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dohavatiuk.nenagera.entity.PocketCurrency;

@Repository
public interface PocketCurrencyRepository extends JpaRepository<PocketCurrency, Long>{

}
