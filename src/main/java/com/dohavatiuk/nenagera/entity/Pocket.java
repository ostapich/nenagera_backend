package com.dohavatiuk.nenagera.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "pocket")
public class Pocket {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne(
            cascade = { 
                    CascadeType.DETACH, 
                    CascadeType.MERGE, 
                    CascadeType.PERSIST,
                    CascadeType.REFRESH 
                })
    @JoinColumn(name = "currency_id")
    private PocketCurrency currency;

    @OneToMany(mappedBy = "pocket", 
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private List<Operation> operation;

    @ManyToMany(fetch = FetchType.LAZY, 
                cascade = { 
                        CascadeType.PERSIST,
                        CascadeType.MERGE, 
                        CascadeType.DETACH,
                        CascadeType.REFRESH 
                        })
    @JoinTable(name = "customer_pocket_relations",
               joinColumns = @JoinColumn(name = "pocket_id"),
               inverseJoinColumns = @JoinColumn(name = "customer_id"))
    private List<Customer> customers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PocketCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(PocketCurrency currency) {
        this.currency = currency;
    }

    public List<Operation> getOperation() {
        return operation;
    }

    public void setOperation(List<Operation> operation) {
        this.operation = operation;
    }

}
