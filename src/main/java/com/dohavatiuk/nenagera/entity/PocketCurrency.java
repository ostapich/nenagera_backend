package com.dohavatiuk.nenagera.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "pocket_currency")
public class PocketCurrency {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "currency",
               fetch=FetchType.LAZY,
               cascade = { 
                    CascadeType.DETACH, 
                    CascadeType.MERGE, 
                    CascadeType.PERSIST,
                    CascadeType.REFRESH 
                })
    @JsonBackReference
    private List<Pocket> pockets;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public List<Pocket> getPockets() {
        return pockets;
    }

    public void setPockets(List<Pocket> pockets) {
        this.pockets = pockets;
    }

    public void addPocket(Pocket pocket) {
        if (pockets == null) {
            pockets = new ArrayList<>();
        }

        pockets.add(pocket);
        pocket.setCurrency(this);
    }

}
