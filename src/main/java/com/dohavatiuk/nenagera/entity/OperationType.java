package com.dohavatiuk.nenagera.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "operation_type")
public class OperationType {
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@NotBlank
	@Column(name = "operation_type")
	private String operationType;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="operationType")
	private List<Operation> operations;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	
	public void addOperation(Operation operation) {
		if (operations == null) {
			operations = new ArrayList<>();
		}
		
		operations.add(operation);
		operation.setOperationType(this);
	}
	
}