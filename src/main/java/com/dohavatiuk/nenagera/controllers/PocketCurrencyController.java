package com.dohavatiuk.nenagera.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dohavatiuk.nenagera.entity.PocketCurrency;
import com.dohavatiuk.nenagera.exception.ResourceNotFoundException;
import com.dohavatiuk.nenagera.repository.PocketCurrencyRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class PocketCurrencyController {

    @Autowired
    private PocketCurrencyRepository pocketCurrencyRepository;

    @GetMapping("/currencyList")
    public Page<PocketCurrency> getCurrencies(Pageable pageable) {

        return pocketCurrencyRepository.findAll(pageable);
    }
    
    @GetMapping("/currency/{currencyId}")
    public PocketCurrency getCurrencies(@PathVariable Long currencyId) {

        return pocketCurrencyRepository.findById(currencyId).map((pocketCurrency) -> {
            return pocketCurrency;
        }).orElseThrow(() -> new ResourceNotFoundException("Currency is not found by id " + currencyId));
    }
    
    @PostMapping("/currency")
    public PocketCurrency createCurrency(@Valid @RequestBody PocketCurrency pocketCurrency) {
        
        return pocketCurrencyRepository.save(pocketCurrency);
    }
    
    @PutMapping("/currency/{currencyId}")
    public PocketCurrency updateCurrency(@PathVariable Long currencyId, @Valid @RequestBody PocketCurrency pocketCurrencyTmp) {
        
        return pocketCurrencyRepository.findById(currencyId).map((pocketCurrency) -> {
            pocketCurrency.setName(pocketCurrencyTmp.getName());
            return pocketCurrencyRepository.save(pocketCurrency);
        }).orElseThrow(() -> new ResourceNotFoundException("Currency is not found by id " + currencyId));
    }
    
    @DeleteMapping("/currency/{currencyId}")
    public ResponseEntity<?> deleteCurrency(@PathVariable Long currencyId) {
        
        return pocketCurrencyRepository.findById(currencyId).map((pocketCurrency) -> {
            pocketCurrencyRepository.delete(pocketCurrency);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Currency is not found by id " + currencyId));
    }
}
