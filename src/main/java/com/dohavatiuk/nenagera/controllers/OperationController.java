package com.dohavatiuk.nenagera.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dohavatiuk.nenagera.controllers.viewmodel.OperationViewModel;
import com.dohavatiuk.nenagera.entity.Customer;
import com.dohavatiuk.nenagera.entity.Operation;
import com.dohavatiuk.nenagera.entity.OperationType;
import com.dohavatiuk.nenagera.entity.Pocket;
import com.dohavatiuk.nenagera.exception.ResourceNotFoundException;
import com.dohavatiuk.nenagera.repository.CustomerRepository;
import com.dohavatiuk.nenagera.repository.OperationRepository;
import com.dohavatiuk.nenagera.repository.OperationTypeRepository;
import com.dohavatiuk.nenagera.repository.PocketRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class OperationController {

    @Autowired
    private OperationRepository operationRepository;

    @Autowired
    private OperationTypeRepository operationTypeRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PocketRepository pocketRepository;

    @GetMapping("/operationList")
    public Page<Operation> getOperations(Pageable pageable) {
        
        return operationRepository.findAll(pageable);
    }

    @GetMapping("/operation/{operationId}")
    public Operation getOperation(@PathVariable Long operationId) {

        return operationRepository.findById(operationId).map((operation) -> {
            return operation;
        }).orElseThrow(() -> new ResourceNotFoundException("Operation is not found by id " + operationId));
    }

    @PostMapping("/operation")
    public Operation createOperation(@Valid @RequestBody OperationViewModel operationTmp) {
        Operation saveOperation = new Operation();
        saveOperation.setDescription(operationTmp.getDescription());
        saveOperation.setAmount(operationTmp.getAmount());
        saveOperation.setId(operationTmp.getId());
//        System.out.println("The id is: " + operationTmp.getOperationType());

        if (operationTmp.getOperationType() != null) {
            OperationType operationType = operationTypeRepository.findById(operationTmp.getOperationType())
                    .map((operationTypeTmp) -> {
                        return operationTypeTmp;
                    }).orElseThrow(() -> new ResourceNotFoundException(
                            "Operation is not found by id " + operationTmp.getOperationType()));
            saveOperation.setOperationType(operationType);
        }

        if (operationTmp.getCustomer() != null) {
            Customer customer = customerRepository.findById(operationTmp.getCustomer()).map((customerTmp) -> {
                return customerTmp;
            }).orElseThrow(() -> new ResourceNotFoundException("Customer is not found " + operationTmp.getCustomer()));
            saveOperation.setCustomer(customer);
        }

        if (operationTmp.getPocket() != null) {
            Pocket pocket = pocketRepository.findById(operationTmp.getPocket()).map((pocketTmp) -> {
                return pocketTmp;
            }).orElseThrow(() -> new ResourceNotFoundException("Customer is not found " + operationTmp.getPocket()));
            saveOperation.setPocket(pocket);
        }
//        System.out.println("The id is: " + saveOperation.toString());
//        System.out.println("The description is " + saveOperation.getDescription());
        return operationRepository.save(saveOperation);
    }

    @PutMapping("/operation/{operationId}")
    public Operation updateOperation(@PathVariable Long operationId, @Valid @RequestBody Operation tmpOperation) {

        return operationRepository.findById(operationId).map((operation) -> {
            operation.setAmount(tmpOperation.getAmount());
            operation.setDescription(tmpOperation.getDescription());
            return operationRepository.save(operation);
        }).orElseThrow(() -> new ResourceNotFoundException("Operation is not found by id " + operationId));
    }

    @DeleteMapping("/operation/{operationId}")
    public ResponseEntity<?> deleteOperation(@PathVariable Long operationId) {

        return operationRepository.findById(operationId).map((operation) -> {
            operationRepository.delete(operation);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Operation is not found by id " + operationId));
    }
}
