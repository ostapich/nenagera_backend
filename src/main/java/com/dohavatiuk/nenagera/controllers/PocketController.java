package com.dohavatiuk.nenagera.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dohavatiuk.nenagera.controllers.viewmodel.PocketViewModel;
import com.dohavatiuk.nenagera.entity.Pocket;
import com.dohavatiuk.nenagera.entity.PocketCurrency;
import com.dohavatiuk.nenagera.exception.ResourceNotFoundException;
import com.dohavatiuk.nenagera.repository.PocketCurrencyRepository;
import com.dohavatiuk.nenagera.repository.PocketRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class PocketController {

    @Autowired
    private PocketRepository pocketRepository;
    
    @Autowired
    private PocketCurrencyRepository pocketCurrencyRepository;

    @GetMapping("/pocketList")
    public Page<Pocket> getPockets(Pageable pageable) {

        return pocketRepository.findAll(pageable);
    }

    @GetMapping("/pocket/{pocketId}")
    public Pocket getPocket(@PathVariable Long pocketId) {
        
        return pocketRepository.findById(pocketId).map((pocket) -> {
            return pocket;
        }).orElseThrow(() -> new ResourceNotFoundException("Pocket is not found by id " + pocketId));
    }

    @PostMapping("/pocket")
    public Pocket createPocket(@Valid @RequestBody PocketViewModel tmpPocket) {

        Pocket savePocket = new Pocket();
        savePocket.setId(tmpPocket.getId());
        savePocket.setName(tmpPocket.getName());
        System.out.println("" + tmpPocket.toString());
        
        PocketCurrency currency = pocketCurrencyRepository.findById(tmpPocket.getCurrency()).map((pocketCurrency) -> {
            return pocketCurrency;
        }).orElseThrow(() -> new ResourceNotFoundException("Currency is not found by id " + tmpPocket.getCurrency()));
        savePocket.setCurrency(currency);
        
        return pocketRepository.save(savePocket);
    }

    @PutMapping("/pocket/{pocketId}")
    public Pocket updatePocket(@PathVariable Long pocketId, @Valid @RequestBody Pocket tmpPocket) {

        return pocketRepository.findById(pocketId).map((pocket) -> {
            pocket.setName(tmpPocket.getName());
            return pocketRepository.save(pocket);
        }).orElseThrow(() -> new ResourceNotFoundException("Pocket is not found by id " + pocketId));
    }

    @DeleteMapping("/pocket/{pocketId}")
    public ResponseEntity<?> deletePocket(@PathVariable Long pocketId) {

        return pocketRepository.findById(pocketId).map((pocket) -> {
            pocketRepository.delete(pocket);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Pocket is not found by id " + pocketId));
    }

}
