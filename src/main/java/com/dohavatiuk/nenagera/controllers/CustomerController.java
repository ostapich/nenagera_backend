package com.dohavatiuk.nenagera.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dohavatiuk.nenagera.entity.Customer;
import com.dohavatiuk.nenagera.exception.ResourceNotFoundException;
import com.dohavatiuk.nenagera.repository.CustomerRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping("/customerList")
    public Page<Customer> getCustomers(Pageable pageable) {

        return customerRepository.findAll(pageable);
    }

    @GetMapping("/customer/{customerId}")
    public Customer getCustomer(@PathVariable Long customerId) {

        return customerRepository.findById(customerId).map((customer) -> {
            return customer;
        }).orElseThrow(() -> new ResourceNotFoundException("Customer is not found by id " + customerId));
    }

    @PostMapping("/customer")
    public Customer createCustomer(@Valid @RequestBody Customer customer) {

        return customerRepository.save(customer);
    }

    @PutMapping("/customer/{customerId}")
    public Customer updateCustomer(@PathVariable Long customerId, @Valid @RequestBody Customer tmpCustomer) {

        return customerRepository.findById(customerId).map((customer) -> {
            customer.setName(tmpCustomer.getName());
            customer.setPassword(tmpCustomer.getPassword());
            return customerRepository.save(customer);
        }).orElseThrow(() -> new ResourceNotFoundException("Customer is not found by id " + customerId));
    }

    @DeleteMapping("/customer/{customerId}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long customerId) {

        return customerRepository.findById(customerId).map((customer) -> {
            customerRepository.delete(customer);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Customer is not found by id " + customerId));
    }

}