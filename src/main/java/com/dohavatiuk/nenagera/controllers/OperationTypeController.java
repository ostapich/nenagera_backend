package com.dohavatiuk.nenagera.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dohavatiuk.nenagera.entity.OperationType;
import com.dohavatiuk.nenagera.exception.ResourceNotFoundException;
import com.dohavatiuk.nenagera.repository.OperationTypeRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class OperationTypeController {

    @Autowired
    private OperationTypeRepository operationTypeRepository;

    @GetMapping("/operationtypeList")
    public Page<OperationType> getOperationTypes(Pageable pageable) {
        return operationTypeRepository.findAll(pageable);
    }
    
    @GetMapping("/operationtype/{operationTypeId}")
    public OperationType getOperationType(@PathVariable Long operationTypeId) {
        return operationTypeRepository.findById(operationTypeId).map((operationType) -> {
            return operationType;
        }).orElseThrow(() -> new ResourceNotFoundException("Operation type not found id " + operationTypeId));
    }

    @PostMapping("/operationtype")
    public OperationType createOperationType(@Valid @RequestBody OperationType operationType) {
        return operationTypeRepository.save(operationType);
    }

    @PutMapping("operationtype/{operationTypeId}")
    public OperationType updateOperationType(@PathVariable Long operationTypeId,
            @Valid @RequestBody OperationType operationTypeRequest) {

        return operationTypeRepository.findById(operationTypeId).map(operationType -> {
            operationType.setOperationType(operationTypeRequest.getOperationType());
            return operationTypeRepository.save(operationType);
        }).orElseThrow(() -> new ResourceNotFoundException("Operation type not found id " + operationTypeId));

    }

    @DeleteMapping("operationtype/{operationTypeId}")
    public ResponseEntity<?> deleteOperationType(@PathVariable Long operationTypeId) {

        return operationTypeRepository.findById(operationTypeId).map(operationType -> {
            operationTypeRepository.delete(operationType);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Operation type not found id " + operationTypeId));

    }
}