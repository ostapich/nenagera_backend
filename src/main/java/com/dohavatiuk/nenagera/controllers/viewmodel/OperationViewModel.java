package com.dohavatiuk.nenagera.controllers.viewmodel;

import java.math.BigDecimal;


public class OperationViewModel {
    
    private Long id;

    private String description;

    private BigDecimal amount;

    private Long pocket;

    private Long customer;

    private Long operationType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getPocket() {
        return pocket;
    }

    public void setPocket(Long pocket) {
        this.pocket = pocket;
    }

    public Long getCustomer() {
        return customer;
    }

    public void setCustomer(Long customer) {
        this.customer = customer;
    }

    public Long getOperationType() {
        return operationType;
    }

    public void setOperationType(Long operationType) {
        this.operationType = operationType;
    }

}
