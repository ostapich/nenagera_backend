package com.dohavatiuk.nenagera.controllers.viewmodel;

import java.util.List;

import javax.validation.constraints.NotNull;


public class PocketViewModel {

    private Long id;

    @NotNull
    private String name;
    
    @NotNull
    private Long currency;

    private List<Long> operation;

    private List<Long> customers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCurrency() {
        return currency;
    }

    public void setCurrency(Long currency) {
        this.currency = currency;
    }

    public List<Long> getOperation() {
        return operation;
    }

    public void setOperation(List<Long> operation) {
        this.operation = operation;
    }

    public List<Long> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Long> customers) {
        this.customers = customers;
    }

    @Override
    public String toString() {
        return "PocketViewModel [id=" + id + ", name=" + name + ", currency=" + currency + ", operation=" + operation
                + ", customers=" + customers + "]";
    }
    
    
}
